clc, clear, close all,

%% DATI
M = 67000;        %[kg] massa all'atteraggio
v = 60;           %[m/s] velocità inizio frenata
H = 4;            %[m] altezza baricentro
L = 13;           %[m] passo carrello 0______o
rp = 90/100;      %    percentuale peso sul carrello principale (statico)
N = 4;            %    numero ruote frenanti
Dr = 1100 * 1e-3; %[m] diametro ruota
D1 = 650 * 1e-3;  %[m] Dmax disco
D2 = 250 * 1e-3;  %[m] Dmin disco
ut = 0.9;         %    coeff. attrito terreno
re = 80/100;      %    percentuale energia assorbits come calore dischi
Ta = 30;          %[°C] temperatura ambiente
Pp = 1.3 * 1e6;   %[Pa] Pmax sulle pastiglie
Smax = 10 * 1e-3; %[m] spessore massimo pastiglie
Cu = 2000/1e-9;   %[J/m^3] coefficiente usura pastiglie
ra = 80/100;      %Percentuale energia assorbita come calore dai dischi
g = 9.81;         %[m/s^2]
%% ACCIAO - CARBONIO
% Tmax di utilizzo
Tmax_ac = 500;    %[°C] 
Tmax_cb = 800;    %[°C] 
% calore specifico
cs_ac = 500;      %[J/(kg*K)]
cs_cb = 1250;     %[J/(kg*K)]
% densità
ro_ac = 7800;     %[kg/m^3] 
ro_cb = 1700;     %[kg/m^3]
% spessore massimo disco
Sdmax_ac = 10 * 1e-3;%[m] 
Sdmax_cb = 15 * 1e-3;%[m]
% coefficiente attrito dischi
mu_ac = 0.25;     
mu_cb = 0.3;

%% 1) DIMENSIONAMENTO NUMERI DISCHI FRENO

Ec = 1/2 * M * v^2; %[J]
Et = re * Ec; %[J] % re * Ec = md * cs * dT; per ricavare la massa totale
Adr = pi/4 * (D1^2 - D2^2); %[m^2]

% ACCIAO
dT_ac = Tmax_ac - Ta;                             %[°C]
md_ac = (re * Ec)/(cs_ac * dT_ac);                %[Kg] massa totale dei dischi in acciao necessari a frenare
mdr_ac = md_ac / N;                               %[Kg]
Vdr_ac = mdr_ac / ro_ac;                          %[m^3]
Sdr_ac = Vdr_ac / Adr;                            %[m]
Ndr_ac = ceil(Sdr_ac / Sdmax_ac);                 %numero dei dischi effettivo (intero)

Vdr_ac_eff = Adr * (Ndr_ac * Sdmax_ac); % [m^3] volume effettivo dopo approssimazione all'intero
mD_ac = ro_ac * Vdr_ac_eff * N;  %[kg]
Tmax_ac_ = re * Ec / (mD_ac * cs_ac) + Ta; %[°C]

fprintf('ACCIAO:\nN° dischi = %d, Tmax = %g [°C]\n',Ndr_ac,Tmax_ac_);

% CARBONIO
dT_cb = Tmax_cb - Ta;                             %[°C]
md_cb = (re * Ec)/(cs_cb * dT_cb);                %[Kg] massa totale dei dischi in acciao necessari a frenare
mdr_cb = md_cb / N;                               %[Kg]
Vdr_cb = mdr_cb / ro_cb;                          %[m^3]
Sdr_cb = Vdr_cb / Adr;                            %[m]
Ndr_cb = ceil(Sdr_cb / Sdmax_cb);                 %numero dei dischi effettivo (intero)

Vdr_cb_eff = Adr * (Ndr_cb * Sdmax_cb); % [m^3] volume effettivo dopo approssimazione all'intero
mD_cb = ro_cb * Vdr_cb_eff * N;  %[kg]
Tmax_cb_ = re * Ec / (mD_cb * cs_cb) + Ta; %[°C]

fprintf('CARBONIO:\nN° dischi = %d, Tmax = %g [°C]\n',Ndr_cb,Tmax_cb_);
fprintf('\nIl numero di dischi per ruota calcolato va bene\ndato che sono entro il limite termico concesso\n');

%% 2)Determinazione Pressione
Lmg = 0.1 * L; %[m] distanza tra carrello principale e baricentro
Lag = 0.9 * L; %[m]  carrello anteriore e baricentro, 90%        
Nm = (M*g*Lag)/(L+ut*H); %[N] reazione normale sulla ruota principale
Tmax = ut * Nm; %[N] Reazione tangenziale totale
Cfmax = Tmax * (Dr/2); %[N*m] coppia frenante per tutte le ruote
Cfr_max =  Cfmax /4; %[N*m] coppia frenante per singola ruota

%ACCIAO
Cfpastiglia_max_ac = Cfr_max /(2*Ndr_ac); %[N/m]
p_ac = (3*Cfpastiglia_max_ac)/(mu_ac*(2*pi*ra)*((D1/2)^3 - (D2/2)^3)); %[Pa]
fprintf('\nACCIAO:\nPressione = %g [Pa]\n',p_ac);
%CARBONIO
Cfpastiglia_max_cb = Cfr_max /(2*Ndr_cb); %[N/m]
p_cb = (3*Cfpastiglia_max_cb)/(mu_cb*(2*pi*ra)*((D1/2)^3 - (D2/2)^3)); %[Pa]
fprintf('CARBONIO:\nPressione = %g [Pa]\n',p_cb);

fprintf('\nEntrambe le pressioni sono entro il limite di %d[Pa]\n',Pp);

%% 3)Numero Atterraggi
Vp = Et/Cu; %[m^3] Volume Unitario Consumato
%ACCIAO
Vtot_pastiglie_ac = ra * Adr * Smax * 2 * Ndr_ac * N; %[m^3]
Natt_acc = floor(Vtot_pastiglie_ac / Vp);
fprintf('\nACCIAO:\nN° atterraggi = %g \n',Natt_acc);
%CARBONIO
Vtot_pastiglie_cb = ra * Adr * Smax * 2 * Ndr_cb * N; %[m^3]
Natt_cb = floor(Vtot_pastiglie_cb / Vp);
fprintf('CARBONIO:\nN° atterraggi = %g \n',Natt_cb);