global G1 alfa
%alfa=input('esponente della forza =')
alfa=2
%alfa=1.95
%gravitational constant
G=6.673*10^(-11)
% earth mass
Mt=5.98*10^24
%sun mass
Ms=1.98*10^30
%astronomical unity
Lua=1.496*10^11
%period (one year)
Tref=365*24*3600
xc=0.0;yc=0.0;
set(0,'defaultlinelinewidth',2)
G1=G*Ms*Tref^2/Lua^(alfa+1)
%     tmax=input('intervallo di integrazione (in anni) = ')
     tmax=10
%     dt=input('passo temporale = ')
     dt=0.002
     n=tmax/dt;
     t=linspace(0,tmax,n);
%    x0(1)=input('distanza iniziale (in U.A.) = ')
     x0(1)=3;
     x0(2)=0;
%    x0(3)=input('componente x della velocit??? iniziale (Km/sec) = ')
     x0(3)=0;
%    x0(4)=input('componente y della velocit??? iniziale (Km/sec) = ')
     x0(4)=5;
     x0(3)=x0(3)*1000*Tref/Lua;
     x0(4)=x0(4)*1000*Tref/Lua;
     y=lsode('force',x0,t);
%plot(xc,yc,'ro',y(:,1),y(:,2))
 plot(xc,yc,'ro');hold on
delay=0.001;
comet(y(:,1),y(:,2),delay)

