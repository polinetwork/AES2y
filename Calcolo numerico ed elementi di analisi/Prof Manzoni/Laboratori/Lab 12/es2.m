%% Esercizio 2
clc
clear
close all

%% 1 e 2
fun=@(x) sin((1)./(1+x.^2));

a=-2*pi;
b=2*pi;

x_dis=linspace(a,b,1000);
f_dis=fun(x_dis);
figure(1)
plot(x_dis,f_dis,'k')

err_max = [];

for n=[2 4 8 10]
x_nod=linspace(a,b,n+1);
f_nod=fun(x_nod);
P=polyfit(x_nod,f_nod,n);
P_dis = polyval( P, x_dis );
err_max = [ err_max; max( abs( P_dis - f_dis ) ) ];
hold on
plot(x_dis,P_dis)
end
title('Interpolazione f(x)', 'FontSize', 12)
legend('fun','n=2','n=4','n=8','n=10', 'Location', 'BestOutside')

figure(2)
plot([2 4 8 10], err_max, '-s')
title('Nodi equispaziati: errore in norma infinito')
xlabel('n')
ylabel('error')


%% 3

figure(3)
plot(x_dis,f_dis, 'k', 'DisplayName', 'fun');
axis([-2*pi 2*pi -0.1 .9] )
hold on
xlabel('asse x');
ylabel('asse y');
title('Interpolazione nodi Chebyshev', 'FontSize', 12);

 
err_max = [];

N=[2 4 8 10];
for n=N
    k=0:n;
    t=-cos(pi*k/n);           % vettore contenete i nodi di interpolazione
    x_nod=((b-a)/2)*t+(a+b)/2; % trasformazione affine
    f_nod=fun(x_nod);
    P=polyfit(x_nod,f_nod,n);
    poly_dis=polyval(P,x_dis);
    figure(3);
    h1=plot (x_dis,poly_dis, 'DisplayName', ...
        ['inter ' num2str(n) ' nodi']);
    plot (x_nod,f_nod,'o', 'Color', get(h1, 'Color'), ...
        'DisplayName', ['nodi (n = ' num2str(n) ')']);
    err_max= [err_max; max(abs(poly_dis-f_dis))];
    
end

figure(3)
legend( 'show','Location', 'BestOutside');

figure(4)
plot([2 4 8 10], err_max, '-s')
xlabel('n')
ylabel('errore')
title('Errore di interpolazione nodi Chebyshev', 'FontSize', 12)



