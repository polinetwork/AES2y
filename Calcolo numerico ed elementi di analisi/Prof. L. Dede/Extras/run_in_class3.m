[ x ] = fixed_point_iterations( @(x) cos(x), 0.1, 1.1, 0.2, 3, 5, 0 );

% [ x ] = fixed_point_iterations( @(x) 2 * x.^2-1, 0.5, 2.0, 1.1, 2, .1, 0 );

% [ x ] = fixed_point_iterations( @(x) sqrt(x+1)/sqrt(2), 0.5, 2.0, 1.9, 3, 2, 0 );