function [ x ] = fixed_point_iterations( iter_fun, a, b, x0, kmax, fs, flag )
%==========================================================================
%          fixed_point_iterations.m
%
% Display the fixed point iterations.
% Inputs: iter_fun  = function handle for iteration function
%         a, b = real numbers (a<b) specifying the interval
%         x0 = starting point
%         kmax = max number of iterations 
%         fs = viz option
%         flag = if true makes movie
% Output: approximated fixed point after kmax iterations
%--------------------------------------------------------------------------
% L. Dede', EPF Lausanne, October 2, 2012 
%==========================================================================

% checks
if( b <= a )
    error( 'Check input values. We need: b > a ');   
end
if( x0 > b || x0 < a )
    error( 'Check input values. We need: a <= x0 <= b');    
end


% prepare plot
FontSize = 20;
close all;
fig=figure( kmax + 1 );
I = b - a;
fs = round( fs );
fs = max( fs, 0.5 );
a_viz = a - I / fs;
b_viz = b + I / fs;
x_viz = linspace( a_viz, b_viz, 1001 );
iter_fun_viz = iter_fun( x_viz );
plot( x_viz, iter_fun_viz, '-b',x_viz,x_viz,'-r','Linewidth', 2);
axis equal 
axis( [ a_viz b_viz a_viz b_viz ]);
xlabel('x');
ylabel('y');
% title('\Phi(x)=((x+1)/2)^{1/2}, \alpha=1, x^{(0)}=1.9, a=0.5, b=2.0'); %%%%%%
grid on
th = text( a_viz+0.25*(b_viz-b)/2, 1.1 * iter_fun( a ),'\Phi(x)');
set( th, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' );
th = text(b+0.25*(b_viz-b)/2,b-0.75*(b_viz-b)/2,'y=x');
set( th, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' );
set( gca, 'XTick', [ a b ] );
set( gca, 'XTickLabel', [ 'a'; 'b' ] );
set( gca, 'YTick', [ a b ] );
set( gca, 'YTickLabel', [ 'a'; 'b' ] );
set( gca, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' )
tlhand = get( gca, 'title' );
set( tlhand, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' );
xlhand = get( gca, 'xlabel' );
set( xlhand, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' )
ylhand = get( gca, 'ylabel' );
set( ylhand, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' )
hold on

plot( [ a_viz b_viz],[0 0],'-k');
plot( [ 0 0 ], [ a_viz b_viz],'-k');


alpha = x0;
for k = kmax : 100 * kmax   
    alpha = iter_fun( alpha );    
end

if( alpha < b && alpha > a )
    plot( alpha, 0, 'xg','MarkerSize',20,'Linewidth',2);
    plot( [ alpha alpha], [0 alpha], '-g','Linewidth',2);
    th = text( alpha+0.2*(a-a_viz)/2, 0+0.75 * (a-a_viz)/2,'\alpha');
    set( th, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' );

end

if flag
    movobj = VideoWriter( 'example_fixed_point.mp4', 'MPEG-4' );
    open( movobj );
    Nf = 10; % options for short/long movie
    for i = 1 : 3 * Nf
        F = getframe(fig);
        writeVideo( movobj, F );
    end
end

pause
% pause(2)

x = x0;
plot( x0, 0, '.k','MarkerSize',20,'Linewidth',2);
th = text( x0, 0-0.5 * (a-a_viz)/2,'$x^{(0)}$','interpreter','latex');
set( th, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' );

if flag
    for i = 1 : 3 * Nf
        F = getframe(fig);
        writeVideo( movobj, F );
    end       
end

pause
% pause(2)

for k = 0 : kmax
        
    plot( [ x, x], [ x * (k > 0 ), iter_fun( x ) ], '--k','LineWidth',1.5);
    if flag
        for i = 1 : 2 * Nf
            F = getframe(fig);
            writeVideo( movobj, F );
        end       
    end
    
    pause
% pause( 1 )
    plot( [ x, iter_fun(x)], [ iter_fun( x ),iter_fun( x ) ], '--k','LineWidth',1.5);
    if flag
        for i = 1 : 2 * Nf
            F = getframe(fig);
            writeVideo( movobj, F );
        end       
    end
    pause
% pause( 1 )
    plot( [ iter_fun(x),iter_fun(x)], [ iter_fun( x ),0 ], '--k','LineWidth',1.5);
    x = iter_fun( x );    
    plot( x, 0, '.k','MarkerSize',20,'Linewidth',2);
    th = text( x, 0-0.5 * (a-a_viz)/2,strcat(strcat('$x^{(',num2str(k+1)),')}$'),'interpreter','latex');
    set( th, 'FontSize', FontSize, 'FontName', 'TimesNewRoman' );
    if flag
        for i = 1 : 3 * Nf
            F = getframe(fig);
            writeVideo( movobj, F );
        end       
    end
    pause
% pause(2)
    
end

if flag
    close( fig )
    close( movobj );
end



return



