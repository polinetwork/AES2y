function [t,u]=predcor(odefun,tspan,y0,Nh,...
               predictor,corrector,varargin)
%PREDCOR  Risolve equazioni differenziali
%  con il metodo predictor-corrector
%  [T,Y]=PREDCOR(ODEFUN,TSPAN,Y0,NH,@PRED,@CORR) con
%  TSPAN=[T0 TF] integra il sistema di equazioni
%  differenziali y' = f(t,y) dal tempo T0 a TF con
%  condizioni iniziali Y0 utilizzando un generico
%  metodo predictor-corrector su una griglia
%  equispaziata di NH intervalli.
%  La funzione ODEFUN(T,Y) deve ritornare un vettore
%  contenente f(t,y), della stessa dimensione di y.
%  Ogni riga del vettore soluzione Y corrisponde ad
%  un istante temporale del vettore colonna T.
%  Le function PRED e CORR caratterizzano il tipo
%  di metodo predictor-corrector scelto.
%  Possono essere scelte, ad esempio, tra le
%  function proposte:
%  feonestep, beonestep, cnonestep.
%  [T,Y] = PREDCOR(ODEFUN,TSPAN,Y0,NH,...
%                  @PRED,@CORR,P1,..)
%  passa i parametri addizionali P1,P2,... alle
%  function ODEFUN, PRED e CORR come
%  ODEFUN(T,Y,P1,P2,...),
%  PRED(T,Y,P1,P2...),
%  CORR(T,Y,P1,P2...).

h=(tspan(2)-tspan(1))/Nh;
y=y0(:); % genera sempre un vettore colonna
w=y; u=y.';
tt=linspace(tspan(1),tspan(2),Nh+1);
for t=tt(1:end-1)
   fn = odefun(t,w,varargin{:});
   upre = predictor(t,w,h,fn);
   w = corrector(t+h,w,upre,h,odefun,...
          fn,varargin{:});
   u = [u; w.'];
end
t = tt';
end
