%% 1.2

f = @(x) sin((7/2)*x) + exp(x) - 1;
a = 0; b = 1;

I_ex = (2/7)*(1-cos(7/2))+exp(1)-2

I_vect = [];
for n = 0:2
    I = gaussLegendre(f,a,b,n);
    I_vect = [I_vect; I];
end

I_vect
e_vect = I_ex - I_vect


%% 1.3.a
a = 0; b = 1;
r_vect = [1 3 5]

for d = 0:6
    
    fprintf('Polynomial degree: %d', d)
   f = @(x) x.^d;
   
   I_ex = 1/(d+1);
   
   
   I_vect = [];
for n = 0:2
    I = gaussLegendre(f,a,b,n);
    I_vect = [I_vect; I];
end

e_vect = I_ex - I_vect
   
   
end




%% 1.3.b (Comparison with other formulas)
d=6;
a = 0; b = 1;

n = 2;
M = n;

   f = @(x) x.^d;
   
   I_ex = 1/(d+1);
   
   
   % GL
   I_vect = [];
   I = gaussLegendre(f,a,b,n);
   I_vect = [I_vect; I];
   e_vect_Gl = I_ex - I_vect
   
   
   
   % PM
   I_vect = [];
   I = pmedcomp( a, b, M, f );
   I_vect = [I_vect; I];
   e_vect_PM = I_ex - I_vect
   
   
   % T
   I_vect = [];
   I = trapcomp( a, b, M, f );
   I_vect = [I_vect; I];
   e_vect_T = I_ex - I_vect
   
   
   
   % S
   I_vect = [];
   I = simpcomp( a, b, M, f );
   I_vect = [I_vect; I];
   e_vect_S = I_ex - I_vect
