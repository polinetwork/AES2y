
close all

%% 1.1-4

a = 0; b = 0.8;
f = @(x) cosh(x-0.5);
x = [a:0.001:b];

plot(x,f(x))
grid on

V = @(x) pi*(f(x)).^2;

M = 5000;
pmedcomp( a, b, M, V )


trapcomp( a, b, M, V )


simpcomp( a, b, M, V )


%% 1.5


V_PM_vect = [];
V_T_vect = [];
V_S_vect = [];

for M = 1:20
    
    V_PM_vect = [V_PM_vect pmedcomp( a, b, M, V )];
    V_T_vect =  [V_T_vect  trapcomp( a, b, M, V )];
    V_S_vect =  [V_S_vect  simpcomp( a, b, M, V )];
    
end


figure
M_vect = [1:20];

plot(M_vect, V_PM_vect)
hold on
grid on
plot(M_vect, V_T_vect)
plot(M_vect, V_S_vect)

legend('P.M.','T.','S.')



%% 1.6
V_ex = pi*((sinh(1)+sinh(0.6))/4 + 0.4);


err_PM_vect = abs( V_PM_vect - V_ex )
err_T_vect = abs( V_T_vect - V_ex )
err_S_vect = abs( V_S_vect - V_ex )


figure
M_vect = [1:20];


loglog(M_vect, err_PM_vect)
hold on
grid on
loglog(M_vect, err_T_vect)
loglog(M_vect, err_S_vect)

legend('e_{PM}','e_{T}','e_{S}')



%% 1.7

d2f = @(x) 2*pi*cosh(x - 1/2).^2 + 2*pi*sinh(x - 1/2).^2
d4f = @(x) 8*pi*cosh(x - 1/2).^2 + 8*pi*sinh(x - 1/2).^2;
tol = 1e-5;

M_PM = ceil( sqrt( ((b-a)^3)*max(d2f(x))/(24*tol) ) )

M_T = ceil( sqrt( ((b-a)^3)*max(d2f(x))/(12*tol) ) )

M_S = ceil( ( ((b-a)^5)*max(d4f(x))/(2880*tol) ).^(1/4) )

