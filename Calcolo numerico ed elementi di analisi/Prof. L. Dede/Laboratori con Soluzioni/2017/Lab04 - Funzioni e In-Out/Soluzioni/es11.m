clc
clear

a = zeros (100, 1);
k = 8;
r = 2.5 + mod(k,2);
trovato=0;
a (1) = 0.5;
for j=2:100
    a(j) = r * a(j-1) * (1 - a(j-1));
    if abs (a(j) - a(j-1)) < 1e-3 && trovato==0
        trovato = j;
    end
end
figure
plot(1:length(a), a, 'ro-');
if (trovato > 0)
    fprintf ('Elemento trovato!\n');
    fprintf ('Posizione: %d, valore: %f\n', trovato, a (trovato));
else
    disp ('Nessun valore della successione soddisfa la condizione')
end