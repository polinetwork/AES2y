n = 50;

A = 4*eye(n) - diag(ones(n-1,1),-1) - diag(ones(n-1,1),1) - diag(ones(n-2,1),-2) - diag(ones(n-2,1),2);
b = 0.2*ones(n,1);
lambdas = eig(A);

if (min(lambdas) > 0) & (A==A')  %% N.B. Quando ho condizioni su scalari &&, quando su matrici &
    
    disp('SDP')
    
    condA = max(lambdas)/min(lambdas) %%
    
end

P = eye(n);
v = eig(inv(P)*A);

alphaOpt = 2/(max(v)+min(v));
x0 = zeros(n,1);
tol = 1e-5;
nmax = 1e+4;


alpha = 0.2;
[x1,k1] = richardson( A, b, P, x0, tol, nmax, alpha);
k1
B = eye(n) - alpha*inv(P)*A;
rho_B = max(abs(eig(B)))


alpha = 0.33;
[x2,k2] = richardson( A, b, P, x0, tol, nmax, alpha);
k2
B = eye(n) - alpha*inv(P)*A;
rho_B = max(abs(eig(B)))


alpha = alphaOpt;
[x3,k3] = richardson( A, b, P, x0, tol, nmax, alpha);
k3
B = eye(n) - alpha*inv(P)*A;
rho_B = max(abs(eig(B)))


[x4,k4] = richardson( A, b, P, x0, tol, nmax);
k4




%% 5 PARTICULAR CASE -> GAUSS-SEIDEL

alpha = 1;
P = tril(A);
[x5,k5] = richardson( A, b, P, x0, tol, nmax, alpha);
k5
B = eye(n) - alpha*inv(P)*A;
rho_B = max(abs(eig(B)))

M = inv(P)*A;
v = eig(M'*M);
cond2_M = sqrt(max(v)/min(v))

[x5bis,k5bis] = gs ( A, b, x0, tol, nmax);
k5bis




%% 6
P = 2*eye(n) - diag(ones(n-1,1),1) - diag(ones(n-1,1),-1);
alpha = alphaOpt;
[x6,k6] = richardson( A, b, P, x0, tol, nmax, alpha);
k6
B = eye(n) - alpha*inv(P)*A;
rho_B = max(abs(eig(B)))


[x6bis,k6bis] = richardson( A, b, P, x0, tol, nmax);
k6bis




