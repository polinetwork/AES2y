function [t_h, u_h, k_fp_vect] = beuler(f,t_f,y_0,h) %passo solo t_f poichè assumo di partire sempre da t_0 = 0;


t_h = [0:h:t_f];

u_0 = y_0;

u_h = [u_0];
N_h = (t_f-0)/h;

k_fp_vect = [];


itermax = 100;
tol = 1e-5;

for n = 1:N_h
    
    t_np1 = t_h(n+1);

    u_n = u_h(end);

    phi = @(u_np1) u_n +  h*f(t_np1, u_np1);
    [u_np1, k_fp] = fixedPt(u_n, phi, itermax, tol);
    
    u_h = [u_h u_np1];
    k_fp_vect = [k_fp_vect k_fp];
    
end


