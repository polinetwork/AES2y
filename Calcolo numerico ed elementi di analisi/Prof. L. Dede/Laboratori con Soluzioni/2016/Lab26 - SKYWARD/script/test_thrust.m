function [ T_coeffs ] = test_thrust( )
%Void function that loads the two motor tests and retrieve the polynomial
%interpolation (IV order)

%Loading data
test1= matfile('data1');
test2= matfile('data2');

%Interpolation (IV Order)
T_coeffs = polyfit([test1.test1_t;test2.test2_t],...
    [test1.test1_y_fil;test2.test2_y_fil],4);

%Printing Thrust Mean Value using the trapz functions to integrate for the
%mean value theorem
fprintf('Thrust mean value %3.2f\n\n', ...
    trapz(0:0.01:2.1,polyval(T_coeffs,0:0.01:2.1)/2.1));


end

