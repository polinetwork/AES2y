% The MIT License (MIT)
% 
% Copyright (c) 2014, Skyward Experimental Rocketry
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Parameters

clear all
close all
clc

settings.Iyyf=3.98; %Inertia to y-axis (Full)
settings.Iyye=3.33; %Inertia to y-axis (Empty)
settings.C = 90e-3; %Caliber (Fuselage Diameter)
settings.S = settings.C^2/4*pi; %Cross-sectional Surface
settings.ms = 8; %Dry Mass (No Propellant)
settings.tb = 2.1; %Burning Time
settings.mp = 2; %Mass of Fuel
settings.mfr = settings.mp/settings.tb; %Mass Flow Rate
settings.m0 = settings.ms+settings.mp; %Total Starting Mass
%%% T contains the polynomial coefficients of the interpolating Lagrange
%%% poly.
%settings.T = 780;
settings.T = test_thrust(); %Thrust
% settings.CM = 0;
% settings.CMA = -50; %CMA
% settings.CD = 0.8; %CD
% settings.CLA = 3.222; %CLA

%For Aero Coefficients I'm passing all the table
settings.Aerofull = matfile('Aerofull.mat');
settings.Aeroempty = matfile('Aeroempty.mat');
settings.Idot = 1/settings.tb*(settings.Iyyf-settings.Iyye);

%Wind settings 
settings.vwind0=[-1 0];
settings.n = 0.1;
settings.z0 = 1200;

time=0:0.01:180;
X0 = [0 0 85*pi/180 0 0 0 settings.m0 settings.Iyyf];
options = odeset('AbsTol',1E-6,'Events',@(t,Y) apogee(t,Y,settings));
[T,Y] = ode45(@(t,Y) missile_complete(t,Y,settings),time,X0,options);

figure
plot(T,Y(:,2),'k','Linewidth',2)
title('Altitude Profile')
figure
plot(T,(Y(:,3)-atan(Y(:,5)./Y(:,4)))*180/pi,'k','Linewidth',2);
title('AoA')

figure
plot(T,Y(:,3)*180/pi,'k','Linewidth',2);
title('Theta');
figure 
plot(T,Y(:,4:5),'k','Linewidth',2)
title('Velocit�');

figure
plot(T,Y(:,6),'k','Linewidth',2)
title('Angular Rate p');

