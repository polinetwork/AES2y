

%% esercizio - Prima parte
clear all
close all

% Punto 1
N = [5:20];
K = [];
Kprec=[];
Itnp = [];
Itp = [];
for n = N;

    one = ones(n,1);

    A = spdiags([2*one, one, 4*one, one, 2*one], -2:2, n,n);
    KA = condest(A);  % si usa condest per le matrici sparse 
    K = [K KA];
    b = ones(n,1);
   
    toll = 1e-6;
    nmax = 5*1e3;
    P = tril(A);
    Kprec=[Kprec, condest(inv(P)*A)]

    %P = spdiags([one, 2*one, one], -1:1, n,n);
    x0 = zeros(n,1);
    
    % metodo del gradiente
    [xnp, knp] = richardson(A, b, eye(n), x0, toll, nmax);
    Itnp = [Itnp knp];
    
    % metodo del gradiente precondizionato
    [xp, kp] = richardson(A, b, P, x0, toll, nmax);
    Itp = [Itp kp];
end

% Punto 2
figure(1);
semilogy(N, Itnp, 'r', N, Itp, 'b')
grid on
title('confronto metodo del gradiente VS gradiente precondizionato')
xlabel('dimensioni sistema');
ylabel('iterazioni')
legend('gradiente', 'gradiente precondizionato', 'Location', 'Northwest')

% Punto 3
figure(2);
plot(N, K, 'r')
grid on
title('Numero di condizionamento di A')
xlabel('dimensioni sistema');
ylabel('cond(A)')

%Punto 3bis
figure(4)
plot(N, Kprec, 'r','Linewidth',3)
grid on
title('Numero di condizionamento di P^{-1}A','FontSize',20)
xlabel('dimensioni sistema','FontSize',20);
ylabel('cond(P^{-1}A)','FontSize',20)

%% Punto 5
% vedi conjgrad.m


%% esercizio - Seconda parte

N = [5:20];
Itcnp = [];

for n = N;
    
    one = ones(n,1);

    A = spdiags([2*one, one, 4*one, one, 2*one], -2:2, n,n);
  
    b = ones(n,1);
   
    toll = 1e-6;
    nmax = 5000;
    x0 = ones(n,1);
    
    [xcnp, kcnp] = conjgrad(A, b, x0, toll, nmax);
    Itcnp = [Itcnp kcnp];
end

% Punto 7
figure(3);
semilogy( N, Itnp, 'r', N, Itp, 'b',N, Itcnp, 'g-')
grid on
title('confronto metodo del gradiente coniugato VS gradiente e gradiente precondizionato')
xlabel('dimensioni sistema');
ylabel('iterazioni')
legend( 'gradiente','gradiente precondizionato','gradiente coniugato', 'Location', 'Northwest')

