clear all; close all;

%se attivo questo parametro salvo le immagini che produco
salva=0;

%creo due autovettori ortonormali, che uso per creare la A sdp
v1=([4 3]/5)';
v2=([-3 4]/5)';

%questi sono i due autovalori associati
l1=5;
l2=10;

%questa ? la matrice A
A=l1*v1*v1'+l2*v2*v2';

%se cambio gli autovalori la forma quadratica associata ? pi? schiacciata
%(NB autoval ~ 1/semiasse perci? autoval piccolo =>  semiasse lungo ), come
%si vede qualche riga sotto, quando disegniamo le forme quadratiche Phi e
%Phi2
l1=1;
l2=10;

A2=l1*v1*v1'+l2*v2*v2';



%questo ? il termine noto della forma quadratica che vogliamo minimizzare
b=[4 8]';
%b=[0 0]';

%definisco la griglia 
x=linspace(-10,10,80);
y=linspace(-10,10,80);

[X,Y]=meshgrid(x,y);

%definisco le forme quadratiche Phi e Phi2
Phi = 0.5* ( A(1,1)*X.^2 + A(2,2)*Y.^2 + 2*A(1,2)*X.*Y ) - b(1)*X - b(2)*Y;
Phi2 = 0.5* ( A2(1,1)*X.^2 + A2(2,2)*Y.^2 + 2*A2(1,2)*X.*Y ) - b(1)*X - b(2)*Y;


%confrontiamo graficamente le due forme quadratiche 

%1) questo crea il grafico 3D
surf(X,Y,Phi,'Lines','no');
title('phi 1')

hold on
%2) aggiungo al grafico 3D le linee di livello; l'ultimo parametro d?? il numero di linee di livello
%desiderate
contour(X,Y,Phi,20)
%salvo l'immagine su file
if (salva==1) , saveas(gcf,'phi1_surf.eps','epsc2'), end


%rifaccio lo stesso per la seconda forma quadratica

figure;
surf(X,Y,Phi2,'Lines','no');
title('phi 2')
hold on;
contour(X,Y,Phi2,20)
if (salva==1), saveas(gcf,'phi2_surf.eps','epsc2'), end

%per rendersi bene conto del fatto che la seconda forma quadratica ?? pi??
%schiacciata la cosa migliore ?? confrontare le linee di livello delle due
%forme quadratiche

figure
contour(X,Y,Phi,20)
title('phi 1 - linee di livello')
if (salva==1), saveas(gcf,'phi1_contour.eps','epsc2'), end

figure
contour(X,Y,Phi2,20)
title('phi 1 - linee di livello')
if (salva==1), saveas(gcf,'phi2_contour.eps','epsc2'), end




%% risolvo il problema di minimizzazione della forma phi2 con il metodo di Richardson stazionario (alpha scelto in modo da convergere)

x0=[-9 -9]';
nmax=1000;
toll=1e-7;
[Sg_rich,n_it_rich] = grad(A2,b,x0,nmax,toll,0.05);


%% plotto la storia di convergenza: premere invio per visualizzare i vari passi


figure;
contour(X,Y,Phi2,80)
title('\alpha = 0.05')
%con questo comando il grafico avr?? la stessa scala per l'asse x e per l'asse y
axis equal
hold on;
for i=1:8%n_it_rich
    plot(Sg_rich(1,i:i+1),Sg_rich(2,i:i+1),'-or','LineWidth',2,'MarkerSize',4,'MarkerFaceColor','r')
    pause
end
for i=9:n_it_rich
    plot(Sg_rich(1,i:i+1),Sg_rich(2,i:i+1),'-or','LineWidth',2,'MarkerSize',4,'MarkerFaceColor','r')
    %pause
end
pause

if (salva==1), saveas(gcf,'Rich_conv.eps','epsc2'), end

%% risolvo il problema di minimizzazione della forma phi2 con il metodo di Richardson stazionario (alpha scelto in modo da divergere)

%close all

x0=[-9 -9]';
nmax=1000;
toll=1e-7;
[Sg_rich2,n_it_rich2] = grad(A2,b,x0,nmax,toll,0.24);


%% plotto la storia di convergenza: premere invio per visualizzare i vari passi


figure;
contour(X,Y,Phi2,80)
title('\alpha = 0.24')
axis equal
hold on;
for i=1:4%n_it_rich2
    plot(Sg_rich2(1,i:i+1),Sg_rich2(2,i:i+1),'-or','LineWidth',2)
    pause
end
pause
if (salva==1), saveas(gcf,'Rich_div.eps','epsc2'), end



%% risolvo il problema di minimizzazione della forma quadratica phi2 con il metodo del gradiente

%close all

x0=[-9 -9]';
nmax=100;
toll=1e-7;
[Sg2,n_it2] = grad(A2,b,x0,nmax,toll,NaN);





%% plotto la storia di convergenza: premere invio per visualizzare i vari passi


figure;
contour(X,Y,Phi2,80)
title('Gradiente')
axis equal
hold on;
for i=1:8%n_it2
    plot(Sg2(1,i:i+1),Sg2(2,i:i+1),'-or','LineWidth',2)
    pause
end
for i=9:n_it2
    plot(Sg2(1,i:i+1),Sg2(2,i:i+1),'-or','LineWidth',2)
    %pause
end
pause
if (salva==1), saveas(gcf,'grad.eps','epsc2'), end


%% voglio risolvere il problema di minimizzazione della forma quadratica phi2 con il
%  metodo del gradiente preconiugato, tramite il precondizionatore P

%close all

x0=[-9 -9]';
nmax=100;
toll=1e-7;

%P ? una matrice "simile" ad A2: al posto di avere l2=10 usiamo l2=5
P=(l1*v1*v1'+5*v2*v2');



%quello che vogliamo ottenere quindi ? una nuova forma quadratica Phiprec meno
%schiacciata, ma che condivida il minimo con Phi2; 

%in effetti la forma quadratica precondizionata ? meno schiacciata, ma ha
%lo stesso minimo

Aprec=P\A2;
eig(Aprec)
bprec=P\b;

Phiprec = 0.5* ( Aprec(1,1)*X.^2 + Aprec(2,2)*Y.^2 + 2*Aprec(1,2)*X.*Y ) - bprec(1)*X - bprec(2)*Y;


figure
contour(X,Y,Phi2,80)
grid on
title('phi 2 - linee di livello')
if (salva==1), saveas(gcf,'phi2_contour_conf.eps','epsc2'), end

figure
contour(X,Y,Phiprec,80)
grid on
title('phi prec - linee di livello')
if (salva==1), saveas(gcf,'phiprec_contour_conf.eps','epsc2'), end

%% risolvo con il gradiente precondizionato

[Sg_prec,n_it_prec] = grad(A2,b,x0,nmax,toll,NaN,P);






%% plotto la storia di convergenza

%close all


figure;
contour(X,Y,Phi2,60)
axis equal
hold on;
title('Gradiente Precondiz. vs Gradiente')
%disegno in grigio le direzioni del gradiente normale
 for i=1:n_it2-1
     plot(Sg2(1,i:i+1),Sg2(2,i:i+1),'--','LineWidth',2,'Color',[0.48 0.48 0.48])
     %pause
 end

 
%poi sovrappongo in rosso quelle del gradiente precondizionato
for i=1:10%n_it_prec
    plot(Sg_prec(1,i:i+1),Sg_prec(2,i:i+1),'-or','LineWidth',2)
    pause
end
for i=11:n_it_prec
    plot(Sg_prec(1,i:i+1),Sg_prec(2,i:i+1),'-or','LineWidth',2)
    %pause
end
pause
if (salva==1), saveas(gcf,'phi_gradprec.eps','epsc2'), end



%% applicare il gradiente precondizionato ? equivalente ad applicare il
% gradiente non precondizionate alla Phiprec? No! 



[Sg_phiprec,n_it_phiprec] = grad(Aprec,bprec,x0,nmax,toll,NaN);


figure;
contour(X,Y,Phiprec,60)
axis equal
title('Gradiente su P^{-1}A')
hold on;
for i=1:8%n_it_phiprec
    plot(Sg_phiprec(1,i:i+1),Sg_phiprec(2,i:i+1),'-or','LineWidth',2)
    pause
end
for i=9:n_it_phiprec
    plot(Sg_phiprec(1,i:i+1),Sg_phiprec(2,i:i+1),'-or','LineWidth',2)
end
pause
if (salva==1), saveas(gcf,'phi_precgrad.eps','epsc2'), end


%% verifiche di ortogonalit??

%direzioni del gradiente precondizionato
dir_prec=Sg_prec(:,2:5)-Sg_prec(:,1:4)

%direzioni del gradiente applicato al sistema precondizionato
dir_phiprec=Sg_phiprec(:,2:5)-Sg_phiprec(:,1:4)

%le direzioni del gradiente applicato al sistema precondizionato devono essere ortogonali
dir_phiprec(:,1)'*dir_phiprec(:,2)
dir_phiprec(:,2)'*dir_phiprec(:,3)
dir_phiprec(:,3)'*dir_phiprec(:,4)

%mentre quelle del gradiente precondizionato in generale non lo saranno ...
dir_prec(:,1)'*dir_prec(:,2)
dir_prec(:,2)'*dir_prec(:,3)
dir_prec(:,3)'*dir_prec(:,4)

%... ma sono a due a due P ortogonali:
dir_prec(:,1)'*P*dir_prec(:,2)
dir_prec(:,2)'*P*dir_prec(:,3)
dir_prec(:,3)'*P*dir_prec(:,4)

%direzioni non consecutive invece in generale non saranno P ortogonali  
dir_prec(:,1)'*P*dir_prec(:,3)
dir_prec(:,1)'*P*dir_prec(:,4)
dir_prec(:,2)'*P*dir_prec(:,4)


%% possiamo fare di meglio, ed avere tutte direzioni ortogonali?? usiamo il gradiente coniugato

[Sg_conj,n_it_conj] = conjgrad_iter(A2,b,x0,nmax,toll);


figure;
contour(X,Y,Phi2,60)
axis equal
hold on;
title('Gradiente Coniugato vs Precondizionato')


%plotto in grigio le iter di gradiente precondizionato
for i=1:n_it_prec
    plot(Sg_prec(1,i:i+1),Sg_prec(2,i:i+1),'--','LineWidth',2,'Color',[0.48 0.48 0.48])
    %pause
end


%poi in rosso quelle di grad coniugato
for i=1:n_it_conj
    plot(Sg_conj(1,i:i+1),Sg_conj(2,i:i+1),'-or','LineWidth',2)
    pause
end


if (salva==1), saveas(gcf,'phi_conj.eps','epsc2'), end



