%%
K=100;
L=20;
N=20;
n=N-1;
%costruisco la matrice
extradiag=ones(n-1,1);
maindiag=-2*ones(n,1);
A=diag(maindiag,0)+diag(extradiag,1)+diag(extradiag,-1);
A=K*A;
%costruisco il termine noto 
t_noto=zeros(n,1);
%aggiungo delle forzanti esterne
t_noto=16*rand(n,1);
t_noto(end)=t_noto(end)-K*L;

%risolvo con \ e con thomas
x_mat=A\t_noto;
[LL,UU,x_thom]=thomas(A,t_noto);

%confronto le soluzioni
%figure
%plot(abs(x_mat-x_thom),'-o');

%plotto la soluzione
x=x_thom;
figure
plot(x,'-o','LineWidth',2,'MarkerFaceColor','b')
title('posizione','FontSize',16)


figure
plot(x(2:end)-x(1:end-1),'-o','LineWidth',2,'MarkerFaceColor','b')
title('allungamenti','FontSize',16)

figure;
plot([t_noto(1:end-1); t_noto(end)+K*L],'-o','LineWidth',2,'MarkerFaceColor','b')
title('forzanti','FontSize',16)
