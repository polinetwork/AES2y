% Esercizio 2
clear all
close all

fprintf( '\n######## Esercizio 2 ########\n')

%%% PUNTO 1 %%%

a=-5; b=5;
fRunge= @(x)  1./(1+ x.^2);
x=linspace ( a , b , 1000);
y=fRunge ( x ) ;
figure( 1 )
plot ( x , y , ' g ' )
title ( ' Funzione e Approssimanti ' )
hold on

for n=[5,10]; % grado del polinomio
    h=(b-a ) / n ; % distanza t r a i nodi
    xnodi =[ a : h : b ] ; % v e t t o r e di n+1 nodi e q u i s p a z i a t i

    ynodi =fRunge ( xnodi ) ;
    grado=length ( xnodi )-1;
    P=polyfit ( xnodi , ynodi , grado ) ;
    xgraf=linspace ( a , b , 1000);
    ygraf =polyval (P, xgraf) ;
    figure(1);
    if n==5
        plot ( xgraf , ygraf , ' r ' , xnodi , ynodi , ' or ' )
        legend(' fun ',' Equi5 ',' Nodi Equi5 ');
    elseif n==10
        plot ( xgraf , ygraf , ' k ' , xnodi , ynodi , ' ok ' )
        legend(' fun ',' Equi5 ',' Nodi Equi5 ',' Equi10 ',' Nodi Equi10 ');
    end

    % calcolo dell'errore commesso
    errore =abs ( y-ygraf ) ;
    figure(2)
    hold on
    if n==5
        plot ( xgraf , errore , ' r ' )
        title ( ' Errore di Interpolazione ' )
        legend(' Equi5 ');
        fprintf('\n Press any key to continue...\n');pause;
    elseif n==10
        plot ( xgraf , errore , ' k ' )
        legend(' Equi5 ',' Equi10 ');
    end
end

fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 2 %%%

for n=[5,10]
    k =[ 0: n ] ;
    t=-cos ( pi*k / n ) ;
    xnodi =(( b-a ) / 2 )*t +(a+b ) / 2 ;

    ynodi =fRunge ( xnodi ) ;
    grado=length ( xnodi )-1;
    P= polyfit( xnodi , ynodi , grado ) ;
    ygraf =polyval (P, xgraf) ;
    figure(1) 
    if n==5
        plot ( xgraf , ygraf , 'r--' , xnodi , ynodi , 'r+' )
        legend( ' fun ' , ' Equi5 ' , ' Nodi Equi5 ',' Equi10 ',' Nodi Equi10 ',...
            'Cheb5','Nodi Cheb5');
    elseif n==10
        plot ( xgraf , ygraf , 'k--' , xnodi , ynodi , 'k+' )
        legend( ' fun ' , ' Equi5 ' , ' Nodi Equi5 ',' Equi10 ',' Nodi Equi10 ',...
            'Cheb5','Nodi Cheb5','Cheb10','Nodi Cheb10');
    end

    % calcolo dell'errore commesso
    errore =abs ( y-ygraf ) ;
    figure( 2 )
    if n==5
        plot ( xgraf , errore , 'r--' )
        legend(' Equi5 ',' Equi10 ','Cheb5');
        fprintf('\n Press any key to continue...\n');pause;
    elseif n==10
        plot ( xgraf , errore , 'k--' )
        legend(' Equi5 ',' Equi10 ','Cheb5','Cheb10');
    end

end

fprintf('\n Press any key to continue...\n');pause;

%%% PUNTO 3 %%%

for n=[5,10]
    h=(b-a ) / n ;
    xnodi =[ a : h : b ] ;
    ynodi =fRunge ( xnodi ) ;
    ygraf = spline ( xnodi , ynodi , xgraf) ;
    figure(1)
    if n==5 
        plot ( xgraf , ygraf , 'r:' )
        legend( ' fun ' , ' Equi5 ' , ' Nodi Equi5 ',' Equi10 ',' Nodi Equi10 ',...
                'Cheb5','Nodi Cheb5','Cheb10','Nodi Cheb10','Spline5');
    elseif n==10
        plot ( xgraf , ygraf , 'k:' )
        legend( ' fun ' , ' Equi5 ' , ' Nodi Equi5 ',' Equi10 ',' Nodi Equi10 ',...
                'Cheb5','Nodi Cheb5','Cheb10','Nodi Cheb10','Spline5','Spline10');
    end

    % calcolo dell'errore commesso
    errore =abs ( y-ygraf) ;
    figure(2)
    if n==5
        plot ( xgraf , errore , 'r:' )
        legend(' Equi5 ',' Equi10 ','Cheb5','Cheb10','Spline5');
        fprintf('\n Press any key to continue...\n');pause;
    elseif n==10
        plot ( xgraf , errore , 'k:' )
        legend(' Equi5 ',' Equi10 ','Cheb5','Cheb10','Spline5','Spline10');
    end
end


