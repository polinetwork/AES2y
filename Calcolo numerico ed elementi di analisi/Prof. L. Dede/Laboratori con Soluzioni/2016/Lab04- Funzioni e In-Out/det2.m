function [det]=det2(A)
% Laboratorio 4 Esercizio 1
[n,m]=size(A);
if n==m
    if n==2
        det=A(1,1)*A(2,2)-A(2,1)*A(1,2);
    else
        error('Solo matrici 2x2');
    end
else 
    error('Solo matrici quadrate');
end
return     
    