%% ESERCIZIO 1

% costruzione dei vettori

v_1=2.^(0:10)
v_2=cos(pi./(1:10))'
v_3=0.1./(2.^(0:5))

% suggerimento:  6=5*1+1;
%               11=5*2+1;
%               51=5*10+1;
v_4(1:2:19) =  exp(1:10) + (-1).^(2:11).*((1:10)*5+1)

%% ESERCIZIO 2

% costruzione della matrice

A=2*diag(ones(5,1))+...
  5*diag(ones(4,1),-1)+10*diag(ones(3,1),-2)+40*diag(1,-4)+...
  10*diag(ones(3,1),2)+40*diag(1,4)

% somma di tutti gli elementi della matrice A
sumA=sum(A(:))
% oppure
sumA=sum(sum(A))

% estrazione delle sottomatrici richieste
A_1=A(1:3,1:3)
A_2=A(1:2:5,[1,2,4])
A_3=A(3:5,[2,4,5])

%% ESERCIZIO 3

% costruzione della matrice B

% con 3 istruzioni

B_3=eye(10);
B_3(2:end-1,[1 end])=1;
B_3([1 end],2:end-1)=1;

B_3

% con 2 istruzioni

B_2(1:10,[1,10]) = [0,0 ; ones(8,2); 0,0]; 
B_2 = B_2 + B_2' + diag(ones(10,1));

B_2

% con 1 istruzione

B_1 = [[ones(1,9),0] ; [ones(8,1),eye(8),ones(8,1)]; [0,ones(1,9)]];

B_1

% SE B fosse una matrice tutta di 1 con l'antidiagonale nulla..non servono
% necessariamente i cicli.. ma:

% con 2 istruzioni

Banti_2=eye(10);
Banti_2=ones(10)-Banti_2(:,end:-1:1);

Banti_2

% con 3 istruzioni
clear B

Banti_1=ones(10)-rot90(eye(10));

Banti_1
% costruzione della matrice C

C=diag(1:200)+...
  diag(ones(199,1),-1)+0.5*diag(ones(198,1),-2)+...
  diag(ones(199,1),+1)+0.5*diag(ones(198,1),+2);

% stampa di alcune sottomatrici per verificare che sia stata costruita bene

C_1=C(1:5,1:5)
C_100=C(floor(end/2):floor(end/2)+4,floor(end/2):floor(end/2)+4)
C_196=C(end-4:end,end-4:end)  


% costruzione della matrice D
D=diag(20:-2:2)+0.5*diag(ones(8,1),-2)+diag(3.^(0:8),+1)
