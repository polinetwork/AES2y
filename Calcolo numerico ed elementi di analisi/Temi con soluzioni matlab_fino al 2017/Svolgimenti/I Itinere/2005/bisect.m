function [x_vect,k] = bisect(a,b,tol,fun)


x = (a+b)/2;

x_vect = [];

k = 0;

if (fun (a) * fun (b) > 0)
    error ('La funzione deve avere segno diverso nei due estremi');
end


err = tol+1;

while err > tol
    
    k = k+1;
    
    if fun(x) == 0
        return
    elseif fun(a)*fun(x) < 0
        b = x;
    else
        a = x;
    end
    
    x = (a+b)/2;

    x_vect = [x_vect; x];
    
    err = (b-a)/2;
    
end


 fprintf(' Radice calcolata : %.8f \n', x_vect(end))


end

