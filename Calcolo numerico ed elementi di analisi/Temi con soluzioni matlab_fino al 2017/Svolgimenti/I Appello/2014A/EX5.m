close all
clear all

f = @(x) 4*(pi^2)*sin(2*pi*x);
u_ex = @(x) sin(2*pi*x) + 1;
alpha = 1;
beta = 1;
x0 = 1; xf = 2;


%% 2

x_axis = [x0:0.001:xf];
plot(x_axis, u_ex(x_axis));
grid on
hold on

h = 0.1;
N = (xf-x0)/h - 1;
knots_int = [x0+h:h:xf-h]';

A = (1/(h^2)) * (diag(-ones(N-1,1),-1) + 2*eye(N) + diag(-ones(N-1,1),1) );
b = f(knots_int);
b(1) = b(1) + (alpha/(h^2));
b(end) = b(end) + (beta/(h^2));

u = A\b;
u = [alpha; u; beta]

x = [x0; knots_int; xf];
plot(x, u)



%% 3

x_axis = [x0:0.001:xf];
figure
plot(x_axis, u_ex(x_axis));
grid on
hold on


h_vect = 0.1*2.^-[0:3];
e_h_vect = [];

for i = 1:length(h_vect)
    
    h = h_vect(i);
    N = (xf-x0)/h - 1;
    knots_int = [x0+h:h:xf-h]';
    
    A = (1/(h^2)) * (diag(-ones(N-1,1),-1) + 2*eye(N) + diag(-ones(N-1,1),1) );
    b = f(knots_int);
    b(1) = b(1) + (alpha/(h^2));
    b(end) = b(end) + (beta/(h^2));
    
    u = A\b;
    u = [alpha; u; beta];
    
    x = [x0; knots_int; xf];
    plot(x, u)
    
    e_h = max(abs(u_ex(x) - u));
    e_h_vect = [e_h_vect e_h];
    
end

legend('u_{ex}(x)', 'u_{h=0.1}', 'u_{h=0.05}', 'u_{h=0.025}', 'u_{h=0.0125}')



figure
loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect.^2,'--k')
legend('e_{h}','h^{2}')



