lambda = -42;
y0 = 2;
t0 = 0;
tf = 10;
f = @(t,y) lambda*y;

y_ex = @(t) y0*exp(lambda*(t-t0));

h_vect = [0.05 0.047 0.0167];

t_axis = [t0:0.001:tf];

figure
d = length(h_vect);
subplot(1,d+1,1)
plot(t_axis, y_ex(t_axis))
grid on
title('Exact Solution')

h_max = 2/abs(lambda) % For guaranteeing Heun's method convergence


for i = 1:length(h_vect)
   
    h = h_vect(i);
    
    [t_h, u_h] = heun(f, t0, tf, y0, h);
   
    
    subplot(1,d+1,i+1)
    plot(t_axis, y_ex(t_axis))
    grid on
    hold on
    plot(t_h, u_h)
    legend('y_{ex}', 'u_{h}')

    
    if h<h_max
        title('Convergence')
    else
        title('Divergence')
    end
    
end


